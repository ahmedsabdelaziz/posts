import { PropsWithChildren } from 'react';
import { Button, Layout, Row, Typography } from 'antd';
import { useRouter } from 'next/router';
import style from './AppLayout.module.css';

export default function AppLayout({ children }: PropsWithChildren<{}>) {
  const router = useRouter();

  return (
    <Layout className={style.container}>
      <Layout.Header>
        <Row justify="space-between" align="middle">
          <Typography.Title level={1} className={style['header-items']}>
            Posts Demo
          </Typography.Title>
          <Button
            type="text"
            className={style['header-items']}
            onClick={async () => {
              await fetch('/api/logout', {
                method: 'POST',
              });
              router.push('/login');
            }}
          >
            Logout
          </Button>
        </Row>
      </Layout.Header>
      <Layout.Content>{children}</Layout.Content>
    </Layout>
  );
}
