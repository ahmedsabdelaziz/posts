import Link from 'next/link';
import type { User, Post } from '@prisma/client';
import { Button, Card, List, Popconfirm, Row, Skeleton, Typography } from 'antd';
import style from './PostsList.module.css';

interface Props {
  currentUserId: User['id'];
  isLoading: boolean;
  isAdding: boolean;
  posts: Post[];
  loadingPostIds: Post['id'][];
  loadMorePosts: () => void;
  addPost: () => void;
  editPost: (id: Post['id']) => void;
  deletePost: (id: Post['id']) => void;
}

export default function PostsList({
  currentUserId,
  isLoading,
  isAdding,
  posts,
  loadingPostIds,
  loadMorePosts,
  addPost,
  editPost,
  deletePost,
}: Props) {
  return (
    <Card
      className={style['list-container']}
      onScroll={({ currentTarget }) => {
        if (
          // Element is scrollable
          currentTarget.scrollHeight > currentTarget.clientHeight &&
          // Element is scrolled to bottom
          currentTarget.offsetHeight + currentTarget.scrollTop >= currentTarget.scrollHeight
        ) {
          loadMorePosts();
        }
      }}
    >
      <List
        bordered
        header={
          <Row justify="space-between" align="middle">
            <Typography.Title level={3}>Posts</Typography.Title>
            <Button type="primary" loading={isAdding} onClick={() => addPost()}>
              Add new post
            </Button>
          </Row>
        }
      >
        {posts.map((post) => {
          const isPostLoading = loadingPostIds.includes(post.id);

          return (
            <List.Item
              key={post.id}
              actions={
                post.userId == currentUserId
                  ? [
                      <Button key="edit" loading={isPostLoading} onClick={() => editPost(post.id)}>
                        Edit
                      </Button>,
                      post.userId == currentUserId && (
                        <Popconfirm
                          key="delete"
                          title="Are you sure you want to delete this post?"
                          onConfirm={() => {
                            deletePost(post.id);
                          }}
                          okText="Yes"
                          cancelText="No"
                          okButtonProps={{ danger: true }}
                        >
                          <Button key="delete" danger loading={isPostLoading}>
                            Delete
                          </Button>
                        </Popconfirm>
                      ),
                    ]
                  : [
                      <Link key="read" href={`/posts/${post.id}`}>
                        <a>Read</a>
                      </Link>,
                    ]
              }
            >
              <Skeleton key={post.id} title={false} loading={isPostLoading} active>
                <List.Item.Meta title={post.title} />
              </Skeleton>
            </List.Item>
          );
        })}
        {isLoading && (
          <>
            <List.Item>
              <Skeleton title={false} loading active>
                <List.Item.Meta title="" description="" />
              </Skeleton>
            </List.Item>
            <List.Item>
              <Skeleton title={false} loading active>
                <List.Item.Meta title="" description="" />
              </Skeleton>
            </List.Item>
            <List.Item>
              <Skeleton title={false} loading active>
                <List.Item.Meta title="" description="" />
              </Skeleton>
            </List.Item>
          </>
        )}
      </List>
    </Card>
  );
}
