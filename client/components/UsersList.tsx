import { useEffect } from 'react';
import type { User } from '@prisma/client';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Menu, Avatar } from 'antd';
import { ProfileOutlined, SolutionOutlined } from '@ant-design/icons';
import useUsers from '../../client/reducers/users';

interface Props {
  currentUser: Omit<User, 'hashedPassword'>;
}

export default function UsersList({ currentUser }: Props) {
  const router = useRouter();
  const { users, fetchUsers } = useUsers();

  useEffect(() => {
    fetchUsers();
  }, []);

  return (
    <Menu mode="vertical" theme="dark" selectedKeys={[(router.query.user as string) || '']}>
      <Menu.Item key="" icon={<ProfileOutlined size={24} />}>
        <Link href="/">
          <a>All Posts</a>
        </Link>
      </Menu.Item>
      <Menu.Item key="me" icon={<SolutionOutlined size={24} />}>
        <Link href="?user=me">
          <a>My Posts</a>
        </Link>
      </Menu.Item>
      {users
        .filter(({ id }) => id != currentUser.id)
        .map(({ id, name }) => (
          <Menu.Item
            key={id}
            icon={
              <Avatar size={24}>
                {name
                  .split(' ')
                  .slice(0, 2)
                  .map((s) => s[0].toUpperCase())
                  .join('')}
              </Avatar>
            }
          >
            <Link href={`?user=${id}`}>
              <a>{name}</a>
            </Link>
          </Menu.Item>
        ))}
    </Menu>
  );
}
