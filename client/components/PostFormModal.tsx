import type { Post } from '@prisma/client';
import { Modal, Form, Input } from 'antd';
import { useLayoutEffect } from 'react';

interface Props {
  isOpen: boolean;
  initialValues?: Post;
  close: () => void;
  submit: (data: Post) => void;
}

export default function PostFormModal({ isOpen, initialValues, close, submit }: Props) {
  const [form] = Form.useForm();
  useLayoutEffect(() => {
    form.setFieldsValue(initialValues);
  }, [form, initialValues]);

  return (
    <Modal
      title={initialValues ? 'Edit post' : 'Add post'}
      width="80vw"
      visible={isOpen}
      onCancel={() => {
        close();
      }}
      onOk={() => {
        form.submit();
      }}
    >
      <Form
        form={form}
        onFinish={(values) => {
          submit(values);
        }}
      >
        <Form.Item name="title" label="Title">
          <Input />
        </Form.Item>
        <Form.Item name="body" label="Body">
          <Input.TextArea rows={15} />
        </Form.Item>
      </Form>
    </Modal>
  );
}
