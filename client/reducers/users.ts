import { Reducer, useReducer } from 'react';
import { User } from '@prisma/client';
import { GetAllUsersResponse } from '../../pages/api/users';

type UserData = Omit<User, 'hashedPassword'>;

const useUsers = () => {
  const [{ isLoading, users }, dispatch] = useReducer(reducer, state);

  const fetchUsers = async () => {
    dispatch({ type: 'fetch_start' });

    const response = await fetch('/api/users');
    const { data: users } = (await response.json()) as GetAllUsersResponse;

    dispatch({ type: 'fetch_success', users });
  };

  return {
    isLoading,
    users,
    fetchUsers,
  };
};

const state: State = {
  isLoading: false,
  users: [],
};

const reducer: Reducer<State, Action> = (state, action): State => {
  return handlers[action.type]?.(state, action as any) || state;
};

const handlers: ActionHandlers = {
  fetch_start: () => ({ isLoading: true, users: [] }),
  fetch_success: (_state, { users }) => ({ isLoading: false, users }),
};

type State = {
  isLoading: boolean;
  users: UserData[];
};

type FetchStartAction = {
  type: 'fetch_start';
};
type FetchSuccessAction = {
  type: 'fetch_success';
  users: UserData[];
};
type Action = FetchStartAction | FetchSuccessAction;

type ActionHandlers = {
  [key in Action['type']]: (state: State, action: Extract<Action, { type: key }>) => State;
};

export default useUsers;
