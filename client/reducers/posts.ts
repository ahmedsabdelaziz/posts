import { Reducer, useReducer, useEffect, useRef } from 'react';
import type { User, Post } from '@prisma/client';
import type { GetAllPostsResponse, CreatePostResponse } from '../../pages/api/posts';
import type { UpdatePostResponse } from '../../pages/api/posts/[id]';

const usePosts = (userId?: User['id']) => {
  // Initial value as -1 to trigger the initial load even if the userId is undefined
  const previousUserIdRef = useRef<User['id'] | undefined | null>(-1);
  const [{ isLoading, isAdding, posts, totalCount, loadingPostIds }, dispatch] = useReducer(
    reducer,
    initialState,
  );

  useEffect(() => {
    if (previousUserIdRef.current != userId) {
      dispatch({ type: 'initial_fetch_start' });

      fetchPosts(0, 15, userId).then(({ data, totalCount }) => {
        previousUserIdRef.current = userId;
        dispatch({ type: 'initial_fetch_succss', totalCount, posts: data });
      });
    }
  }, [userId]);

  const loadMorePosts = async () => {
    if (isLoading || totalCount > posts.length) {
      return;
    }

    dispatch({ type: 'load_more_start' });

    const { data, totalCount: newTotalCount } = await fetchPosts(posts.length, 5, userId);

    dispatch({ type: 'load_more_success', totalCount: newTotalCount, posts: data });
  };

  const createPost = async (data: Omit<Post, 'id'>) => {
    dispatch({ type: 'create_post_start' });

    const response = await fetch('/api/posts', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
    const created = (await response.json()) as CreatePostResponse;

    dispatch({ type: 'create_post_success', created });
  };

  const updatePost = async (id: Post['id'], data: Partial<Post>) => {
    dispatch({ type: 'update_post_start', id });

    const response = await fetch(`/api/posts/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
    const updated = (await response.json()) as UpdatePostResponse;

    dispatch({ type: 'update_post_success', id, updated });
  };

  const deletePost = async (id: Post['id']) => {
    dispatch({ type: 'delete_post_start', id });

    await fetch(`/api/posts/${id}`, {
      method: 'DELETE',
    });

    dispatch({ type: 'delete_post_success', id });
  };

  return {
    isLoading,
    isAdding,
    posts,
    totalCount,
    loadingPostIds,
    loadMorePosts,
    createPost,
    updatePost,
    deletePost,
  };
};

const fetchPosts = async (skip: number = 0, limit: number, userId?: User['id']) => {
  let search = new URLSearchParams({
    skip: String(skip),
    limit: String(limit),
  });
  if (userId) {
    search.set('userId', String(userId));
  }

  const response = await fetch(`/api/posts?${search.toString()}`);

  return (await response.json()) as GetAllPostsResponse;
};

const initialState: State = {
  isLoading: true,
  isAdding: false,
  totalCount: 0,
  posts: [],
  loadingPostIds: [],
};

const reducer: Reducer<State, Action> = (state, action): State => {
  return handlers[action.type]?.(state, action as any) || state;
};

const handlers: ActionHandlers = {
  initial_fetch_start: () => initialState,
  initial_fetch_succss: (state, { posts, totalCount }) => ({
    ...state,
    isLoading: false,
    posts,
    totalCount,
  }),
  load_more_start: (state) => ({ ...state, isLoading: true }),
  load_more_success: (state, { posts, totalCount }) => ({
    ...state,
    isLoading: false,
    posts: state.posts.concat(posts),
    totalCount,
  }),
  create_post_start: (state) => ({
    ...state,
    isAdding: true,
  }),
  create_post_success: (state, { created }) => ({
    ...state,
    isAdding: false,
    posts: [created, ...state.posts],
  }),
  update_post_start: (state, { id }) => ({
    ...state,
    loadingPostIds: state.loadingPostIds.concat(id),
  }),
  update_post_success: (state, { id, updated }) => ({
    ...state,
    posts: state.posts.map((post) => {
      if (post.id != id) {
        return post;
      }

      return updated;
    }),
    loadingPostIds: state.loadingPostIds.filter((postId) => postId != id),
  }),
  delete_post_start: (state, { id }) => ({
    ...state,
    loadingPostIds: state.loadingPostIds.concat(id),
  }),
  delete_post_success: (state, { id }) => ({
    ...state,
    posts: state.posts.filter((post) => post.id != id),
    loadingPostIds: state.loadingPostIds.filter((postId) => postId != id),
  }),
};

type State = {
  isLoading: boolean;
  isAdding: boolean;
  posts: Post[];
  loadingPostIds: Post['id'][];
  totalCount: number;
};

type InitialFetchStartAction = {
  type: 'initial_fetch_start';
};
type InitialFetchSuccessAction = {
  type: 'initial_fetch_succss';
  totalCount: number;
  posts: Post[];
};
type LoadMoreStartAction = {
  type: 'load_more_start';
};
type LoadMoreSuccessAction = {
  type: 'load_more_success';
  totalCount: number;
  posts: Post[];
};
type CreatePostStartAction = {
  type: 'create_post_start';
};
type CreatePostSuccessAction = {
  type: 'create_post_success';
  created: Post;
};
type UpdatePostStartAction = {
  type: 'update_post_start';
  id: Post['id'];
};
type UpdatePostSuccessAction = {
  type: 'update_post_success';
  id: Post['id'];
  updated: Post;
};
type DeletePostStartAction = {
  type: 'delete_post_start';
  id: Post['id'];
};
type DeletePostSuccessAction = {
  type: 'delete_post_success';
  id: Post['id'];
};
type Action =
  | InitialFetchStartAction
  | InitialFetchSuccessAction
  | LoadMoreStartAction
  | LoadMoreSuccessAction
  | CreatePostStartAction
  | CreatePostSuccessAction
  | UpdatePostStartAction
  | UpdatePostSuccessAction
  | DeletePostStartAction
  | DeletePostSuccessAction;

type ActionHandlers = {
  [key in Action['type']]: (state: State, action: Extract<Action, { type: key }>) => State;
};

export default usePosts;
