import type { NextPage, GetStaticPaths, GetStaticProps, InferGetStaticPropsType } from 'next';
import type { Post } from '@prisma/client';
import { Card, Typography } from 'antd';
import getAllPosts from '../../server/logic/getAllPosts';
import getPostById from '../../server/logic/getPostById';
import AppLayout from '../../client/components/AppLayout';
import style from './[id].module.css';

export const getStaticPaths: GetStaticPaths = async () => {
  const { data: posts } = await getAllPosts();

  return {
    paths: posts.map(({ id }) => ({ params: { id: String(id) } })),
    fallback: 'blocking',
  };
};

export const getStaticProps: GetStaticProps<{ post: Post }> = async ({ params }) => {
  const post = await getPostById(Number(params!.id));

  return {
    props: {
      post: post!,
    },
    // Regenerate the page if at most once every 60 seconds
    revalidate: 60,
  };
};

const Post: NextPage<InferGetStaticPropsType<typeof getStaticProps>> = ({ post }) => {
  return (
    <AppLayout>
      <div className={style.container}>
        <Typography.Title level={2} className={style.title}>
          {post.title}
        </Typography.Title>
        <Card className={style.card}>
          <Typography.Paragraph className={style.paragraph}>{post.body}</Typography.Paragraph>
        </Card>
      </div>
    </AppLayout>
  );
};

export default Post;
