import { useState } from 'react';
import type { NextPage, GetServerSideProps } from 'next';
import Link from 'next/link';
import * as authCookies from '../server/lib/authCookies';
import { Row, Col, Card, Form, Typography, Input, Button } from 'antd';
import style from './register.module.css';
import { GetAllUsersResponse } from './api/users';
import { useRouter } from 'next/dist/client/router';

export const getServerSideProps: GetServerSideProps = async ({ req: request }) => {
  if (authCookies.get(request)) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};

const Register: NextPage = () => {
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();

  return (
    <Row className={style.container}>
      <Col xs={24} md={16} lg={12} xl={8}>
        <Card title={<Typography.Title>Sign up</Typography.Title>}>
          <Form
            name="login"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            onFinish={async (values) => {
              setIsLoading(true);

              await fetch('/api/register', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify(values),
              });

              router.push('/');
            }}
          >
            <Form.Item
              label="Full name"
              name="name"
              rules={[{ required: true, whitespace: true, message: 'Please enter your name' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Email"
              name="email"
              hasFeedback
              rules={[
                { required: true, message: 'Please enter your email' },
                { type: 'email', message: 'Please enter a valid email' },
                {
                  validator: async (rule, value) => {
                    const search = new URLSearchParams({
                      limit: '0',
                      email: value,
                    });

                    const response = await fetch(`/api/users?${search.toString()}`);
                    const { totalCount } = (await response.json()) as GetAllUsersResponse;

                    if (totalCount > 0) {
                      throw new Error('Email already taken.');
                    }
                  },
                },
              ]}
            >
              <Input type="email" />
            </Form.Item>
            <Form.Item
              label="Password"
              name="password"
              rules={[{ required: true, message: 'Please enter your password' }]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              Already have an account?&nbsp;
              <Link href="/login">
                <a>Sign in</a>
              </Link>
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button htmlType="submit" type="primary" loading={isLoading}>
                Create account
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </Col>
    </Row>
  );
};

export default Register;
