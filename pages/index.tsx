import type { NextPage, GetServerSideProps, InferGetServerSidePropsType } from 'next';
import { useState } from 'react';
import type { User, Post } from '@prisma/client';
import { useRouter } from 'next/router';
import * as authCookies from '../server/lib/authCookies';
import getLoggedInUserData from '../server/logic/getLoggedInUserData';
import { Layout } from 'antd';
import usePosts from '../client/reducers/posts';
import AppLayout from '../client/components/AppLayout';
import UsersList from '../client/components/UsersList';
import PostsList from '../client/components/PostsList';
import style from './index.module.css';
import PostFormModal from '../client/components/PostFormModal';

export const getServerSideProps: GetServerSideProps<{ currentUser: User }> = async ({
  req: request,
}) => {
  const token = authCookies.get(request);

  if (!token) {
    return {
      redirect: {
        destination: '/login',
        permanent: false,
      },
    };
  }

  const currentUser = getLoggedInUserData(token);

  return {
    props: { currentUser },
  };
};

const Home: NextPage<InferGetServerSidePropsType<typeof getServerSideProps>> = ({
  currentUser,
}) => {
  const router = useRouter();
  let userId: User['id'] | undefined;
  if (router.query.user == 'me') {
    userId = currentUser.id;
  } else if (router.query.user) {
    userId = Number(router.query.user);
  }

  const [formModalState, setFormModalState] = useState<{ isOpen: boolean; post?: Post }>({
    isOpen: false,
  });

  const {
    isLoading,
    isAdding,
    posts,
    totalCount,
    loadingPostIds,
    loadMorePosts,
    createPost,
    updatePost,
    deletePost,
  } = usePosts(userId);

  return (
    <AppLayout>
      <Layout hasSider>
        <Layout.Content className={style.content}>
          <PostsList
            currentUserId={currentUser.id}
            isLoading={isLoading}
            isAdding={isAdding}
            posts={posts}
            loadingPostIds={loadingPostIds}
            loadMorePosts={loadMorePosts}
            addPost={() => setFormModalState({ isOpen: true })}
            editPost={(id) =>
              setFormModalState({
                isOpen: true,
                post: posts.find((post) => post.id == id),
              })
            }
            deletePost={deletePost}
          ></PostsList>
        </Layout.Content>
        <Layout.Sider collapsible reverseArrow className={style.sider}>
          <UsersList currentUser={currentUser} />
        </Layout.Sider>
      </Layout>
      <PostFormModal
        isOpen={formModalState.isOpen}
        initialValues={formModalState.post}
        close={() => {
          setFormModalState({
            isOpen: false,
          });
        }}
        submit={(data) => {
          if (!formModalState.post) {
            createPost(data);
            setFormModalState({
              isOpen: false,
            });
          } else {
            updatePost(formModalState.post.id, data);
            setFormModalState({
              isOpen: false,
            });
          }
        }}
      />
    </AppLayout>
  );
};

export default Home;
