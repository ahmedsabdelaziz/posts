import type { AppProps } from 'next/app';
import { AnimatePresence, domAnimation, LazyMotion, m } from 'framer-motion';
import '../styles/globals.css';

function MyApp({ Component, pageProps, router }: AppProps) {
  return (
    <LazyMotion features={domAnimation}>
      <AnimatePresence exitBeforeEnter>
        <m.div
          key={router.route}
          initial={router.isSsr === false && 'initial'}
          animate="animate"
          exit="exit"
          variants={transition.variants}
          transition={transition.transition}
        >
          <Component {...pageProps} />
        </m.div>
      </AnimatePresence>
    </LazyMotion>
  );
}

const transition = {
  variants: {
    initial: {
      opacity: 0,
    },
    animate: {
      opacity: 1,
    },
    exit: {
      opacity: 0,
    },
  },
  transition: {
    duration: 0.3,
  },
};

export default MyApp;
