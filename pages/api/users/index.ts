import type { NextApiRequest, NextApiResponse } from 'next';
import type { User } from '@prisma/client';
import getAllUsers from '../../../server/logic/getAllUsers';

const allowedMethods = ['GET'];

export type GetAllUsersResponse = {
  totalCount: number;
  data: Omit<User, 'hashedPassword'>[];
};

const handler = async (request: NextApiRequest, response: NextApiResponse<GetAllUsersResponse>) => {
  if (!allowedMethods.includes(request.method!)) {
    response.setHeader('Allow', allowedMethods);
    response.status(405).end(`Method ${request.method} Not Allowed`);

    return;
  }

  const { skip, limit, email } = request.query as {
    skip?: string;
    limit?: string;
    email?: string;
  };

  const result = await getAllUsers(Number(skip) || undefined, Number(limit) || undefined, email);

  response.status(200).json(result);
};

export default handler;
