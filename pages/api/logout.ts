import type { NextApiRequest, NextApiResponse } from 'next';
import * as authCookies from '../../server/lib/authCookies';

const allowedMethods = ['POST'];

const handler = async (request: NextApiRequest, response: NextApiResponse<null>) => {
  if (!allowedMethods.includes(request.method!)) {
    response.setHeader('Allow', allowedMethods);
    response.status(405).end(`Method ${request.method} Not Allowed`);

    return;
  }

  authCookies.clear(response);

  response.status(200).end();
};

export default handler;
