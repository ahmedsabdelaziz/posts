import type { NextApiRequest, NextApiResponse } from 'next';
import type { User } from '@prisma/client';
import register from '../../server/logic/register';
import * as authCookies from '../../server/lib/authCookies';

const allowedMethods = ['POST'];

export type RegisterResponse = Omit<User, 'hashedPassword'>;

const handler = async (request: NextApiRequest, response: NextApiResponse<RegisterResponse>) => {
  if (!allowedMethods.includes(request.method!)) {
    response.setHeader('Allow', allowedMethods);
    response.status(405).end(`Method ${request.method} Not Allowed`);

    return;
  }

  const {
    body: { name, email, password },
  } = request;

  const { token, expiresIn, user } = await register(name, email, password);

  authCookies.set(response, token, expiresIn);

  response.status(200).json(user);
};

export default handler;
