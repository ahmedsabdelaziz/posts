import type { NextApiRequest, NextApiResponse } from 'next';
import type { Post } from '@prisma/client';
import * as authCookies from '../../../server/lib/authCookies';
import * as jwt from '../../../server/lib/jwt';
import deletePost from '../../../server/logic/deletePost';
import updatePost from '../../../server/logic/updatePost';

const allowedMethods = ['PUT', 'DELETE'];

export type DeletePostResponse = null;
export type UpdatePostResponse = Post;

type Response = DeletePostResponse | UpdatePostResponse;

const handler = async (request: NextApiRequest, response: NextApiResponse<Response>) => {
  if (!allowedMethods.includes(request.method!)) {
    response.setHeader('Allow', allowedMethods);
    response.status(405).end(`Method ${request.method} Not Allowed`);

    return;
  }

  try {
    const token = authCookies.get(request);
    if (!token) {
      throw new Error('Unauthorized');
    }

    jwt.verify(token);
  } catch {
    response.status(401).end('Unauthorized');
  }

  const { id } = request.query as { id: string };

  if (request.method == 'PUT') {
    const data = request.body as Partial<Post>;
    const result = await updatePost(Number(id), data);

    response.status(200).json(result);
  } else if (request.method == 'DELETE') {
    await deletePost(Number(id));

    response.status(200).end();
  }
};

export default handler;
