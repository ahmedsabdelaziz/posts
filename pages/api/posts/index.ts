import type { NextApiRequest, NextApiResponse } from 'next';
import type { Post } from '@prisma/client';
import * as authCookies from '../../../server/lib/authCookies';
import * as jwt from '../../../server/lib/jwt';
import getAllPosts from '../../../server/logic/getAllPosts';
import createPost from '../../../server/logic/createPost';

const allowedMethods = ['GET', 'POST'];

export type GetAllPostsResponse = {
  totalCount: number;
  data: Post[];
};
export type CreatePostResponse = Post;

type Response = GetAllPostsResponse | CreatePostResponse;

const handler = async (request: NextApiRequest, response: NextApiResponse<Response>) => {
  if (!allowedMethods.includes(request.method!)) {
    response.setHeader('Allow', allowedMethods);
    response.status(405).end(`Method ${request.method} Not Allowed`);

    return;
  }

  try {
    const token = authCookies.get(request);
    if (!token) {
      throw new Error('Unauthorized');
    }

    jwt.verify(token);
  } catch {
    response.status(401).end('Unauthorized');
  }

  if (request.method == 'GET') {
    const { skip, limit, userId } = request.query as {
      skip?: string;
      limit?: string;
      userId?: string;
    };

    const result = await getAllPosts(
      Number(skip) || undefined,
      Number(limit) || undefined,
      Number(userId) || undefined,
    );

    response.status(200).json(result);
  } else if (request.method == 'POST') {
    const data = request.body as Omit<Post, 'id'>;

    const result = await createPost(data);

    response.status(200).json(result);
  }
};

export default handler;
