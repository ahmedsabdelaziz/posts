import type { NextApiRequest, NextApiResponse } from 'next';
import type { User } from '@prisma/client';
import login from '../../server/logic/login';
import * as authCookies from '../../server/lib/authCookies';

const allowedMethods = ['POST'];

export type LoginResponse = Omit<User, 'hashedPassword'>;

const handler = async (request: NextApiRequest, response: NextApiResponse<LoginResponse>) => {
  if (!allowedMethods.includes(request.method!)) {
    response.setHeader('Allow', allowedMethods);
    response.status(405).end(`Method ${request.method} Not Allowed`);

    return;
  }

  const {
    body: { email, password },
  } = request;

  const result = await login(email, password);

  if (!result) {
    response.status(400).end('Incorrect email or password.');

    return;
  }

  const { token, expiresIn, user } = result;

  authCookies.set(response, token, expiresIn);

  response.status(200).json(user);
};

export default handler;
