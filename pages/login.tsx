import { useState } from 'react';
import type { NextPage, GetServerSideProps } from 'next';
import Link from 'next/link';
import * as authCookies from '../server/lib/authCookies';
import { Row, Col, Card, Form, Typography, Input, Button, Alert } from 'antd';
import style from './login.module.css';
import { useRouter } from 'next/router';

export const getServerSideProps: GetServerSideProps = async ({ req: request }) => {
  if (authCookies.get(request)) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
};

const Login: NextPage = () => {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<string | undefined>();

  return (
    <Row className={style.container}>
      <Col xs={24} md={16} lg={12} xl={8}>
        <Card title={<Typography.Title>Sign in</Typography.Title>}>
          <Form
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            autoComplete="off"
            onFinish={async (values) => {
              setIsLoading(true);
              setError(undefined);

              const response = await fetch('/api/login', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify(values),
              });

              if (response.status == 400) {
                const errorMessage = await response.text();
                setIsLoading(false);
                setError(errorMessage);

                return;
              }

              const user = await response.json();
              router.push('/');
            }}
          >
            {error && (
              <Form.Item wrapperCol={{ span: 24 }}>
                <Alert showIcon message={error} type="error" />
              </Form.Item>
            )}

            <Form.Item
              label="Email"
              name="email"
              rules={[
                { required: true, message: 'Please enter your email' },
                { type: 'email', message: 'Please enter a valid email' },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Password"
              name="password"
              rules={[{ required: true, message: 'Please enter your password' }]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              Don&apos;t have an account yet?&nbsp;
              <Link href="/register">
                <a>Create an account</a>
              </Link>
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button htmlType="submit" type="primary" loading={isLoading}>
                Sign in
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </Col>
    </Row>
  );
};

export default Login;
