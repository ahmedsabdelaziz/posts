import type { Post } from '@prisma/client';
import db from '../da';

export default async function updatePost(id: Post['id'], data: Partial<Post>) {
  return db.post.update({
    where: {
      id: id,
    },
    data,
  });
}
