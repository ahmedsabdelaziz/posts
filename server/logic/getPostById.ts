import { Post } from '@prisma/client';
import db from '../da';

export default async function getPostById(id: Post['id']) {
  return await db.post.findFirst({
    where: {
      id,
    },
  });
}
