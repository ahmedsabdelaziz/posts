import { Post } from '@prisma/client';
import db from '../da';

export default async function createPost(data: Omit<Post, 'id'>) {
  return await db.post.create({
    data,
  });
}
