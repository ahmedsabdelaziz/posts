import db from '../da';
import { checkHash } from '../lib/password';
import * as jwt from '../lib/jwt';

export default async function login(email: string, password: string) {
  const data = await db.user.findFirst({
    where: {
      email,
    },
  });

  if (!data) {
    return null;
  }
  const { hashedPassword, ...user } = data;

  const isPasswordValid = checkHash(password, hashedPassword);
  if (!isPasswordValid) {
    return null;
  }

  const token = jwt.generate(user);

  return {
    token,
    expiresIn: jwt.expiresIn,
    user,
  };
}
