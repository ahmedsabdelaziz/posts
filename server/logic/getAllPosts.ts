import { User } from '@prisma/client';
import db from '../da';

export default async function getAllPosts(skip?: number, limit?: number, userId?: User['id']) {
  const where = {
    userId: {
      equals: userId,
    },
  };

  const [data, totalCount] = await Promise.all([
    db.post.findMany({
      where,
      take: limit,
      skip,
      orderBy: {
        id: 'desc',
      },
    }),
    db.post.count({
      where,
    }),
  ]);

  return {
    data,
    totalCount,
  };
}
