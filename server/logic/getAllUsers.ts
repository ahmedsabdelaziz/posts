import db from '../da';

export default async function getAllUsers(skip?: number, limit?: number, email?: string) {
  const where = {
    email: {
      equals: email,
    },
  };

  const [data, totalCount] = await Promise.all([
    db.user.findMany({
      select: {
        id: true,
        name: true,
        email: true,
      },
      where,
      take: limit,
      skip: skip,
    }),
    db.user.count({
      where,
    }),
  ]);

  return {
    data,
    totalCount,
  };
}
