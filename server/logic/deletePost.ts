import { Post } from '@prisma/client';
import db from '../da';

export default async function deletePost(id: Post['id']) {
  await db.post.delete({
    where: {
      id,
    },
  });
}
