import { Post } from '@prisma/client';
import db from '../da';
import { hash } from '../lib/password';
import * as jwt from '../lib/jwt';

export default async function register(name: string, email: string, password: string) {
  const hashedPassword = hash(password);

  const user = await db.user.create({
    data: {
      name,
      email,
      hashedPassword,
    },
    select: {
      id: true,
      name: true,
      email: true,
    },
  });

  if (user.id < 11) {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts?userId=${user.id}`);
    const posts = (await response.json()) as Post[];

    await Promise.all(posts.map((post) => db.post.create({ data: post })));
  }

  const token = jwt.generate(user);

  return {
    token,
    expiresIn: jwt.expiresIn,
    user,
  };
}
