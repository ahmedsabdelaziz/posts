import db from '../da';
import * as jwt from '../lib/jwt';

export default function getLoggedInUserData(token: string) {
  const user = jwt.verify(token);

  return user;
}
