import type { User } from '@prisma/client';
import fs from 'fs';
import jwt, { JwtPayload } from 'jsonwebtoken';

export const expiresIn = 7 * 24 * 60 * 60;
const secretKey = fs.readFileSync('./jwt.key');
const publicKey = fs.readFileSync('./jwt.key.pub');

export function generate(user: Omit<User, 'hashedPassword'>) {
  return jwt.sign(user, secretKey, { expiresIn });
}

export function verify(token: string) {
  const { iat, exp, ...user } = jwt.verify(token, secretKey) as JwtPayload;

  return user as User;
}
