import { randomBytes, pbkdf2Sync } from 'crypto';

const saltSize = 32;
const hashSize = 32;
const iterations = 100;
const digest = 'sha512';

export function hash(password: string) {
  const salt = randomBytes(saltSize).toString('hex');
  const hash = pbkdf2Sync(password, salt, iterations, hashSize, digest).toString('hex');

  return salt + hash;
}

export function checkHash(password: string, hashedPassword: string) {
  const salt = hashedPassword.slice(0, saltSize * 2);
  const hash = hashedPassword.slice(saltSize * 2);

  return hash == pbkdf2Sync(password, salt, iterations, hashSize, digest).toString('hex');
}
