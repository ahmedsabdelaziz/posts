import type { NextApiRequest, NextApiResponse, GetServerSidePropsContext } from 'next';
import { serialize } from 'cookie';

const cookieHeader = 'Set-Cookie';
const cookieKey = 'accessToken';

export function set(response: NextApiResponse, accessToken: string, expiresIn: number) {
  // Set the access_token and refresh_token in cookies, so we don't have to cache it and send it
  // with each request on the client side
  response.setHeader(
    cookieHeader,
    serialize(cookieKey, accessToken, {
      maxAge: expiresIn,
      httpOnly: true,
      secure: true,
      sameSite: true,
      path: '/',
    }),
  );
}

export function get(request: NextApiRequest | GetServerSidePropsContext['req']) {
  const { [cookieKey]: token } = request.cookies;

  return token;
}

export function clear(response: NextApiResponse) {
  response.setHeader(
    cookieHeader,
    serialize(cookieKey, '', {
      maxAge: -1,
      httpOnly: true,
      secure: true,
      sameSite: true,
      path: '/',
    }),
  );
}
